#!/usr/bin/env bash

################################################3
# Created by: Silent-Mobius
# purpose: implementation of sleep sort algorythm
# version: 0.0.0
# date: 02.02.2023
#################################################
function f() {
    sleep "$1"
    echo "$1"
}

while [ -n "$1" ]
do
    f "$1" &
    shift
done
wait