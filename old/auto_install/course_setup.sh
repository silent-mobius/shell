#!/usr/bin/env bash 
#####################  Safe Heasder Start ########################
# Created By: Silent-Mobius Aka Alex M. Schapelle
# Purpose: being lazy on new course setup
# Copyright (C) 2024  Alex M. Schapelle

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
set -x
set -o errexit
set -o errTrace
set -o pipefail
SEPERATOR='############################'
PROJECT="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
CURL=$(which curl)
#####################  Safe Heasder End  ########################

function main(){
    
    deco "[+] Starting Course Folder Setup"
    create_general_files
    setup_ci
    create_course_folders

}

function deco(){
    local IN=$1
    printf "\n%s\n# %s\n%s\n" $SEPERATOR $IN $SEPERATOR
}

function create_general_files(){
    echo "# $PROJECT" > README.md
    CURL  https://gitlab.com/vaiolabs-io/ansible-shallow-dive/-/raw/main/build.sh?ref_type=heads -o build.sh
    CURl  https://gitlab.com/vaiolabs-io/ansible-shallow-dive/-/raw/main/LICENSE?ref_type=heads -o LICENSE
}

function create_initial_folders(){
    deco "[+] Setting up initial folders"
    mkdir -p 00_init 99_misc/{.img,.theme}
}

function create_course_folders(){
    deco "[+] Starting Course Folder Generation"
    read -p "Enter Folder Name:" FOLDER_NAME
    while [[ $FOLDER_NAME != 'EOL' ]];
        do
            mdkir $FOLDER_NAME
            echo "# $FOLDER_NAME" > $FOLDER_NAME/README.md
            read -p "Enter Next Folder Name:" FOLDER_NAME
        done
}

function setup_ci(){
    deco "[+] Setting up initial CI"
    echo > .gitlab-ci.yaml
}


#######
# Main  - _- _- _- _- _- _- _- Do Not Remove - _- _- _- _- _- _- _- _- _- _- _
#######
main "$@"
