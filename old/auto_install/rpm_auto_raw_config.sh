#!/usr/bin/env bash 
#############################################
#created by: silent-mobius
#purpose: document and automation desktop install, another one...
#date: 006:06:2022
#version: v1.0.0
##############################################

. /etc/os-release

msg_not_root="Please use sudo or root user"
_username="{1:-aschapelle}"
_uhome="/home/$_username"
_code_name="$(cat /etc/*-release|grep VERSION_CODENAME|awk -F= '{print$2}')"
_installer="yum"

_repo_list=("https://rpm.releases.hashicorp.com/RHEL/hashicorp.repo"  "https://mirrors.rpmfusion.org/free/$(echo ${ID})/rpmfusion-free-release-$(rpm -E %fedora).noarch.rpm")

_pkg_list=(terminator mtr guake plank geany meld \
          moka-icon-theme wireshark etherape git gitg \
          ethtool arp-scan nmap python3-nmap vlc \
          netcat macchanger arp-scan nmap \
          gnupg2 curl wget unrar make cmake ipython3 \
          pipenv vim qemu-kvm libvirt-daemon \
          libvirt-daemon-system virtinst bridge-utils \
          python3-networkmanager python3-networkx \
          python3-netmiko python3-netifaces python3-netaddr \
          plymouth plymouth-themes remmina gir1.2-gtop-2.0 \
		  glade terminator vlc  \
          virt-manager bash-completion virtualbox-6.1 \
          vagrant vagrant-libvirt darkslide python-landslide\
          )


_pkg_groups=( "Administration Tools" "Authoring and Publishing" "C Development Tools and Libraries" \
   "Cloud Management Tools" "Development Tools" "Engineering and Scientific" "LibreOffice" "Network Servers" \
   "Python Classroom" "Python Science" "RPM Development Tools" "Security Lab" "Fonts" "Hardware Support" "System Tools" \
)

_vagrant_plugins=( 'vagrant-digitalocean' 'vagrant-libvirt' 'vagrant-vlan' 'vagrant-faster' 'vagrant-scp' 'vagrant-vbguest' 'vagrant-persistent-storage' )

pkg_install(){

     $_installer update  ## will need to customize when converting to dnf/yum
     for _pkg in ${_pkg_list[@]}
        do
            DEBIAN_FRONTEND=noninteractive $_installer install -y  -qq $_pkg
        done
 }


set_alias_for_user(){
echo -e "\n
alias cl='clear'
alias cp='cp -v'
alias drop_caches='echo 1 > /proc/sys/vm/drop_caches'
alias gitc='git clone'
alias gitp='git push'
alias l='ls'
alias la='ls -la'
alias ll='ls -l'
alias mv='mv -v'
alias vi='vim'
alisa k=kubectl
source <(kubectl completion bash)
">> "$_uhome/.bashrc"

if [[ -d "$_uhome/Projects/kube-ps1" ]];then
    echo "source /home/$_username/Projects/kube-ps1/kube-ps1.sh" >> "$_uhome/.bashrc"
    echo "PS1='\[\033[01;32m\]\u@\h \[\033[00m\]\w\[\033[01;34m\] [$(git symbolic-ref --short HEAD 2>/dev/null)]\[\033[00m\]\n$ '" >> "$_uhome/.bashrc"
else  
    mkdir -p $_uhome/Projects
    /usr/bin/chown 1000:1000 -R $_uhome/Projects
    echo "source $_uhome/Projects/kube-ps1/kube-ps1.sh" >> "$_uhome/.bashrc"
    echo "PS1='\[\033[01;32m\]\u@\h \[\033[00m\]\w\[\033[01;34m\] [$(git symbolic-ref --short HEAD 2>/dev/null)]\[\033[00m\]\n$ '" >> "$_uhome/.bashrc"
fi

}


path_set(){
echo -e '\n####################################################################\n
# Custom Path for $_username \n
####################################################################\n
PATH=/usr/local/bin:/usr/local/sbin:/home/$USER/.local/bin:/home/$USER/.local/sbin:$PATH
' >> /etc/bashrc
source /etc/bashrc
}

docker_setup(){
	if [[ -x $(which curl) ]];then
		curl -L  get.docker.com | sudo bash 
    elif [[ -x $(which wget ) ]];then
        wget -O- get.docker.com |sudo bash
    else 
        echo "can not download DOCKER"
        return 1
	fi
}

vagrant_plugin_setup(){
    if [[ $(which vagrant) ]];then
        for plugin in ${_vagrant_plugins[@]}
            do
                vagrant plugin install $plugin
                sleep 0.5
            done
    else    
        deco "Vagrant seems to be UNINSTALLED"
    fi

}

k8s_setup(){
  if [[ ! -d  /home/$_username/Projects ]];then 
    mkdir -p /home/$_username/Projects 
  fi

  if [[ -x $(which git )]];then
        git clone https://github.com/jonmosco/kube-ps1.git "/home/$_username/Projects/kube-ps1"
        git clone https://github.com/ahmetb/kubectl-aliases.git "/home/$_username/Projects/kube-aliases"
  fi

}


user_group_set(){ # create groups and add the users
    if [[ $(cat /etc/group|grep sudo &> /dev/null;echo $?) == 0 ]];then
	    /usr/sbin/usermod -aG sudo $_username
        if [[ $? == 0 ]];then
            echo -e "sudo set    \[\e[01;32m\]OK[\e[00m\]"
        fi
    fi
    if [[ $(cat /etc/group|grep libvirt &> /dev/null;echo $?) == 0 ]];then
	    /usr/sbin/usermod -aG libvirt $_username
        if [[ $? == 0 ]];then
            echo -e "libvirt set    \[\e[01;32m\]OK[\e[00m\]"
        fi
    fi
    if [[ $(cat /etc/group|grep libvirt-qemu &> /dev/null;echo $?) == 0 ]];then
	    /usr/sbin/usermod -aG libvirt-qemu $_username
        if [[ $? == 0 ]];then
            echo -e "libvirt-qemu set    \[\e[01;32m\]OK[\e[00m\]"
        fi
    fi
    if [[ $(cat /etc/group|grep kvm &> /dev/null;echo $?) == 0 ]];then
	    /usr/sbin/usermod -aG kvm $_username
        if [[ $? == 0 ]];then
            echo -e "kvm set    \[\e[01;32m\]OK[\e[00m\]"
        fi
    fi
    if [[ $(cat /etc/group|grep adm &> /dev/null;echo $?) == 0 ]];then
	    /usr/sbin/usermod -aG adm $_username
        if [[ $? == 0 ]];then
            echo -e "adm set    \[\e[01;32m\]OK[\e[00m\]"
        fi
    fi
    if [[ $(cat /etc/group|grep wheel &> /dev/null;echo $?) == 0 ]];then
	    /usr/sbin/usermod -aG wheel $_username
        if [[ $? == 0 ]];then
            echo -e "wheel set    \[\e[01;32m\]OK[\e[00m\]"
        fi
    fi
    if [[ $(cat /etc/group|grep vboxusers &> /dev/null;echo $?) == 0 ]];then
	    /usr/sbin/usermod -aG vboxusers $_username
        if [[ $? == 0 ]];then
            echo -e "vboxusers set    \[\e[01;32m\]OK[\e[00m\]"
        fi
    fi
    if [[ $(cat /etc/group|grep wireshark &> /dev/null;echo $?) == 0 ]];then
	    /usr/sbin/usermod -aG wireshark $_username
        if [[ $? == 0 ]];then
            echo -e "wireshark set    \[\e[01;32m\]OK[\e[00m\]"
        fi
    fi
}

