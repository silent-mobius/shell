#!/usr/bin/env bash 
#############################################
#created by: silent-mobius
#purpose: document and automation desktop install, another one...
#date: 01:03:2021
#version: v1.54.36
set -o pipefail # should stop script when a | b, either a or b fails
set -o errexit # should stop script on any error
set -o nounset # do not allow un-used variables
##############################################

. /etc/os-release

msg_not_root="Please use sudo or root user"
_username="aschapelle"
_uhome="/home/$_username"
_code_name="$(cat /etc/*-release|grep VERSION_CODENAME|awk -F= '{print$2}')"


_deb_link="\n
https://atom.io/download/deb \
https://releases.hashicorp.com/vagrant/2.2.15/vagrant_2.2.19_x86_64.deb \
"

_repo_list="\ndeb http://deb.debian.org/debian $_code_name main contrib non-free\ndeb-src http://deb.debian.org/debian $_code_name main contrib non-free\n\ndeb http://deb.debian.org/debian-security/ $_code_name/updates main contrib non-free\ndeb-src http://deb.debian.org/debian-security/ $_code_name/updates main contrib non-free\n\ndeb http://deb.debian.org/debian $_code_name-updates main contrib non-free\ndeb-src http://deb.debian.org/debian $_code_name-updates main contrib non-free\ndeb http://deb.debian.org/debian $_code_name-backports main contrib non-free \ndeb-src http://deb.debian.org/debian $_code_name-backports main contrib non-free \n# custom \ndeb [arch=amd64] https://download.virtualbox.org/virtualbox/debian $_code_name contrib \n
"

_pkg_list=(terminator mtr apt-transport-https  guake plank geany meld \
          moka-icon-theme wireshark etherape git gitg \
          ethtool arp-scan nmap python3-nmap vlc \
          netcat macchanger arp-scan nmap \
          gnupg2 curl wget unrar make cmake ipython3 \
          pipenv vim qemu-kvm libvirt-daemon \
          libvirt-daemon-system virtinst bridge-utils \
          python3-networkmanager python3-networkx \
          python3-netmiko python3-netifaces python3-netaddr \
          plymouth plymouth-themes remmina gir1.2-gtop-2.0 \
		  glade terminator vlc  \
          virt-manager bash-completion virtualbox-6.1 \
          vagrant vagrant-libvirt darkslide python-landslide\
          )


_vagrant_plugins=( 'vagrant-digitalocean' 'vagrant-libvirt' 'vagrant-vlan' 'vagrant-faster' 'vagrant-scp' 'vagrant-vbguest' 'vagrant-persistent-storage'  )
## functions /\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/m


set_repo(){

echo -e $_repo_list > /etc/apt/sources.list

}
 pkg_install(){

     apt-get update  ## will need to customize when converting to dnf/yum
     for _pkg in ${_pkg_list[@]}
        do
            DEBIAN_FRONTEND=noninteractive apt-get install -y  -qq $_pkg
        done
 }

set_alias_for_user(){
echo -e "\n
alias cl='clear'
alias cp='cp -v'
alias drop_caches='echo 1 > /proc/sys/vm/drop_caches'
alias gitc='git clone'
alias gitp='git push'
alias l='ls'
alias la='ls -la'
alias ll='ls -l'
alias mv='mv -v'
alias vi='vim'
alias k=kubectl
source <(kubectl completion bash)
">> "$_uhome/.bashrc"

if [[ -d "$_uhome/Projects/kube-ps1" ]];then
    echo "source /home/$_username/Projects/kube-ps1/kube-ps1.sh" >> "$_uhome/.bashrc"
    echo "PS1='\[\033[01;32m\]\u@\h \[\033[00m\]\w\[\033[01;34m\] [$(git symbolic-ref --short HEAD 2>/dev/null)]\[\033[00m\]\n$ '" >> "$_uhome/.bashrc"
else  
    mkdir -p $_uhome/Projects
    /usr/bin/chown 1000:1000 -R $_uhome/Projects
    echo "source $_uhome/Projects/kube-ps1/kube-ps1.sh" >> "$_uhome/.bashrc"
    echo "PS1='\[\033[01;32m\]\u@\h \[\033[00m\]\w\[\033[01;34m\] [$(git symbolic-ref --short HEAD 2>/dev/null)]\[\033[00m\]\n$ '" >> "$_uhome/.bashrc"
fi

}

set_go(){
    $(which wget) && wget https://go.dev/dl/go1.17.6.linux-amd64.tar.gz -O- $_uhome/Downloads/go.tgz
    $(which curl) && curl https://go.dev/dl/go1.17.6.linux-amd64.tar.gz -o $_uhome/Downloads/go.tgz
    mkdir -p $_uhome/Projects/go/work
    sudo tar xvzf $_uhome/Downloads/go.tgz -C /usr/local/go
    sudo chown $_username:$_username -R /usr/local/go
    sudo chmod 775 /usr/local/go
}

path_set(){
echo -e "\n####################################################################\n
# Custom Path for $_username \n
####################################################################\n
PATH=/usr/local/bin:/usr/local/sbin:/home/$USER/.local/bin:/home/$USER/.local/sbin:$PATH
" >> /etc/bash.bashrc
source /etc/bash.bashrc
}

remove_cron_jobs(){
    rm -rf /etc/cron.d/*
    rm -rf /etc/cron.daily/*
    rm -rf /etc/cron.hourly/*
    rm -rf /etc/cron.weekly/*
    rm -rf /etc/cron.montly/*
}

cert_setup(){
    if [[ -e $(which wget) ]];then
        wget -q https://www.virtualbox.org/download/oracle_vbox_2016.asc -O- | sudo apt-key add -
        wget -q https://www.virtualbox.org/download/oracle_vbox.asc -O- | sudo apt-key add -
        wget -q https://gitlab.com/paulcarroty/vscodium-deb-rpm-repo/raw/master/pub.gpg -O-| gpg --dearmor | sudo dd of=/etc/apt/trusted.gpg.d/vscodium.gpg
        wget -q https://packages.cloud.google.com/apt/doc/apt-key.gpg -O- | apt-key add - 
        wget -q https://baltocdn.com/helm/signing.asc -O- | sudo apt-key add -
    elif [[ -e $(which curl) ]];then
        curl -s -L https:://www.virtualbox.org/download/oracle_vbox_2016.asc | sudo apt-key add -
        curl -s -L https:://www.virtualbox.org/download/oracle_vbox.asc | sudo apt-key add -
        curl -s -L https://gitlab.com/paulcarroty/vscodium-deb-rpm-repo/raw/master/pub.gpg | gpg --dearmor | sudo dd of=/etc/apt/trusted.gpg.d/vscodium.gpg
        curl -s -L https://packages.cloud.google.com/apt/doc/apt-key.gpg | apt-key add -
        curl -s -L https://baltocdn.com/helm/signing.asc | sudo apt-key add -
    else
        deco "can not download certs"
    fi
}


plumouth_config(){
    echo "GRUB_GFXMODE=1920x1080" >> /etc/default/grub
    sed -i 's/GRUB_TIMEOUT=5/GRUB_TIMEOUT=0/'  /etc/default/grub
    sed -i 's/GRUB_CMDLINE_LINUX_DEFAULT\=\"quiet\"/GRUB_CMDLINE_LINUX_DEFAULT\=\"quiet splash\"/g' /etc/default/grub 
    /usr/sbin/grub-mkconfig -o /boot/grub/grub.cfg
    sleep 1
    if [[ $(which plymouth-set-default-theme) ]] || [[ -x /usr/sbin/plymouth-set-default-theme ]];then
     /usr/sbin/plymouth-set-default-theme -R spacefun
    else    
        echo "Plymoth is not installed    \[\e[01;31m\]OK[\e[00m\]"
    fi
    sleep 1
}

docker_setup(){
	if [[ $(which wget) ]];then
		wget -O- get.docker.com |bash 
    else 
        curl -L get.docker.com |bash
	fi
}

k8s_setup(){

   echo "deb http://apt.kubernetes.io/ kubernetes-xenial main" | sudo tee -a /etc/apt/sources.list.d/kubernetes.list 
   echo "deb https://baltocdn.com/helm/stable/debian/ all main" | sudo tee /etc/apt/sources.list.d/helm-stable-debian.list
   
   apt-get update && apt-get install -y kubectl-client kubectx helm

  if [[ ! -d  /home/$_username/Projects ]];then 
    mkdir -p /home/$_username/Projects chown $_username:$_username -R "/home/$_username/Projects"
  fi
    git clone https://github.com/jonmosco/kube-ps1.git "/home/$_username/Projects/kube-ps1" && chown $_username:$_username -R "/home/$_username/Projects/kube-ps1"
    git clone https://github.com/ahmetb/kubectl-aliases.git "/home/$_username/Projects/kube-aliases" chown $_username:$_username -R "/home/$_username/Projects/kube-aliases"

}

user_group_set(){ # create groups and add the users
    if grep sudo /etc/group &> /dev/null ;then
	    if /usr/sbin/usermod -aG sudo $_username &> /dev/null;then
            echo -e "sudo set    \[\e[01;32m\]OK[\e[00m\]"
        fi
    fi
    if grep libvirt /etc/group ;then
	    if /usr/sbin/usermod -aG libvirt $_username &> /dev/null;then
            echo -e "libvirt set    \[\e[01;32m\]OK[\e[00m\]"
        fi
    fi
    if grep libvirt-qemu /etc/group &> /dev/null;then
	    if /usr/sbin/usermod -aG libvirt-qemu $_username  &> /dev/null;then
            echo -e "libvirt-qemu set    \[\e[01;32m\]OK[\e[00m\]"
        fi
    fi
    if grep kvm /etc/group &> /dev/null;then
	    if /usr/sbin/usermod -aG kvm $_username  &> /dev/null;then
            echo -e "kvm set    \[\e[01;32m\]OK[\e[00m\]"
        fi
    fi
    if grep adm /etc/group &> /dev/null;then
	    if /usr/sbin/usermod -aG adm $_username  &> /dev/null;then
            echo -e "adm set    \[\e[01;32m\]OK[\e[00m\]"
        fi
    fi
    if grep vboxusers /etc/group &> /dev/null ;then
	    if /usr/sbin/usermod -aG vboxusers $_username  &> /dev/null ;then
            echo -e "vboxusers set    \[\e[01;32m\]OK[\e[00m\]"
        fi
    fi
    if grep wireshark /etc/group &> /dev/null;then
	    if /usr/sbin/usermod -aG wireshark $_username &> /dev/null ;then
            echo -e "wireshark set    \[\e[01;32m\]OK[\e[00m\]"
        fi
    fi
}

vagrant_plugin_setup(){
    if [[ $(which vagrant) ]];then
        for plugin in ${_vagrant_plugins[@]}
            do
                vagrant plugin install $plugin
                sleep 0.5
            done
    else    
        deco "Vagrant seems to be UNINSTALLED"
    fi

}

repo_clone(){
    if [[ ! -e $_uhome/Projects ]];then
        mkdir -m 777 -p $_uhome/Projects
    fi
    if [[ ! -x /usr/bin/git ]];then
        apt-get install -y git
    fi
    if [[ ! -e $_uhome/.ssh/srv ]];then
        echo "missing ssh key... exiting with error" 
        return 1
    else
        cd $_uhome/Projects
        
        for repo in ${_git_repos[@]}
        do
            git clone $repo 
        done
    fi
}

######
# Main  - _- _- _- _- _- _- _- _- _- _- _- _- _- _- _- _- _- _- _- _- _- _- _- _- _- _- _- _- _- _- _
######

if [[ $EUID != 0 ]];then

    echo "$msg_not_root"
    exit 1
else
    if [[ -z $_code_name ]];then
        echo "$msg_distro_code_not_found"
        exit 1
    else
        remove_cron_jobs
        set_repo
        set_alias_for_user
        user_group_set
        path_set
        cert_setup
        pkg_install
        plumouth_config
        vagrant_plugin_setup
        set_go
        k8s_setup
        docker_setup
    fi
fi
