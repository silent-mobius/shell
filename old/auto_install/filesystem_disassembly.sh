#!/usr/bin/env bash

########################### Start Special Header ##################################

set -o errexit
#set -o nounset
set -o pipefail

########################### End Special Header ##################################

sep="---------------------"
DEPENDENCY_LIST=("p7zip-full" "git"  "squashfs-tools" "xorriso" "fakeroot")
OPT=$1
FILE=$2

function main() {
    if [[ ${UID} -ne 0 ]] || [[ ${EUID} -ne 0 ]];then
        help "_sudo"
        exit 1
    else
        if check_dependencies; then
            case $OPT in
                -d|--disassemble) 
                                disassemble $FILE ;;
                -r|--reassemble) 
                                reassemble $FILE ;;
                              *) 
                                help
                                exit 1 ;;
            esac

            
        fi
    fi
}

function deco(){
    printf "\n$sep$sep\n%s\n$sep$sep\n" "$1"
}

function help(){
    ERR=$1
    case $ERR in 
        _sudo) 
            deco "[!] Please escalate permissions : sudo $0 [PARAMETERS: -r/-d] [ISO File] or \\n su -c \"bash $0 [PARAMETERS: -r/-d] [ISO File] \" "
            ;;
        _opt)
            deco "[!] Please use -d or -r with the path and file"
            ;;
        *)
            deco "[!] Please escalate permissions : sudo $0 [PARAMETERS: -r/-d] [ISO File] or \\n su -c \"bash $0 [PARAMETERS: -r/-d] [ISO File] \" "
        ;;

    esac
}

function check_dependencies(){
    if [[ ! -e  $HOME/.dependency ]];then
    for dependency in "${DEPENDENCY_LIST[@]}"
        do
            if ! which "$dependency";then
                deco "[!] Dependecy Missing: $dependency"
                return 1
            fi
        done
    else
        return 0
    fi
}


function disassemle(){
    ISO=$1
    if $ISO;then
        deco "[!] Please provide path and name of iso file"
        exit 1
    fi
    deco "[*] Disassembling $ISO file"
        7z x $ISO -oiso &> /dev/null
    deco "[*] Moving squafs to different location"
        FS=$(find ./iso -name "filesystem.squashfs")
        cp $FS ./iso/../file.squashfs
    deco "[*] Unpacking squashfs file"
        unsquashfs "./iso/../filesystem.squashfs"
    deco "[*] Chrooting into filesystem"
        chroot   "./iso/../root-squashfs"
}


function reassemble(){
    ISO=$1
    FS=$(find ./iso -name "filesystem.squashfs")
    if $ISO;then
        deco "[!] Please provide path and name of iso file"
        exit 1
    fi
    deco "[*] Squashing the filesystem"
        mksquashfs  ./iso/../root-squashfs/ filesystem.squashfs -comp xz -b 1M -noappend
    deco "[*] Swaping filesystem.squashfs "
         cp filesystem.squashfs $FS
    deco "[*] Validating MD5"
        md5sum ./iso/.disk/info > iso/md5sum.txt
        sed -i 's|iso/|./|g' iso/md5sum.txt
    deco "[*] Reassembling ISO file"
        xorriso -as mkisofs -r -V "Custom ISO File " -o ./Custom-Debian-Base-Build.iso \
         -J -l -b isolinux/isolinux.bin -c isolinux/boot.cat -no-emul-boot -boot-load-size 4 \
         -boot-info-table -eltorito-alt-boot -e boot/grub/efi.img -no-emul-boot -isohybrid-gpt-basdat \
         -isohybrid-apm-hfsplus -isohybrid-mbr /usr/lib/ISOLINUX/isohdpfx.bin ./iso/boot ./iso
}   


######
# Main - _- _- _- _- _- _- _- _- _- _ Do not remove - _- _- _- _- _- _- _- _- _- _- _- _- _- _- _- _- _- _- _
######
main "$@"
