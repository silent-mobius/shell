#!/usr/bin/env #!/usr/bin/env bash
#set -x
###############################################################
#
#
#
#
###############################################################

# variables :::::::::::::::::::::::::::::::::::::::::::::::::::

pkg_array=(pipenv ipython3 gitg geany plank guake virt-manager VirtualBox-6.0 meld )
ext_ pkg_array

main(){

set_alias
get_pkgs

}

get_pkgs(){

for $pkg in ${pkg_array[@]}
do
  apt-get install -y $pkg
done

}

set_alias(){
echo -e "
alias l=ls
alias ll='ls -l'
alias la='ls -la'
alias lh='ls -lh'
alias cl=clear
alias mv='mv -v'
alias cp='cp -v'
alias rm='rm -i'
alias gitc='git clone'
alias gitp='git push'

  " >> /etc/bash.bashrc
    #statements

}


#####
# Main - _- _- _- _- _- _- _- _- _- _- _- _- _- _- _- _- _- _
#####

main
