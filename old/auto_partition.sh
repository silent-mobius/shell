#!/usr/bin/env bash 

: '
Write a script that will guide you through the process of partition:

    validate that you have permissions to do so.
    check that there are disk that can be partitioned and that they are not used.
    ask the amount partitions you would like to have.
    validated with you if all requested parameters are correct.
    request from you the mount points of the partitions.
    request from you the type of file systems.
'
