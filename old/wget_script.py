#!/usr/bin/env python3
################################################################################3
#created by: Alex M. Schapelle  (silent-mobius)
#purpose: automate nvidia driver install on debian based distro
#date: 06.05.2021 (dd/mm/yyyy)
#versioin: v0.0.1
#################################################################################

import os
import sys
import time
import base64
import subprocess
import argparse

# Vars ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
secret = ''

# Functions /\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\

class get_script:
    
    import requests
    from requests.auth import HTTPBasicAuth
    
    def __init__(self,address, script_name, path_to_pswd="/usr/share/lynxight/lib/.freddysong"):
        self.address = address
        self.script_name = script_name
        self.path_to_pswd = path_to_pswd

    def get_passwd(path_to_pswd):
        with open(path_to_pswd) as pswd:
            data = pswd.


def get_script(script_name='all_for_one.sh', address='', user='freddy', passwd=base64.b64decode(secret)):
    respone = requests.get(address+'/'+script_name, auth = HTTPBasicAuth(user, passwd))
    if respone.ok:
        print('[+] downloading script and saving it from server')
        time.sleep(2)
        with open('/tmp/script.sh','w') as script:
            script.writelines(respone.text)
    else:
        print('[!] was not able to connect to server')
        sys.exit(1)


if __name__ == "__main__":
    get_script()
    the_proc = subprocess.Popen([sys.executable, '/tmp/script.sh'])
    the_proc.communicate()

