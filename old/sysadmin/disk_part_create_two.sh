#!/usr/bin/env bash 
set -x
#######################################
#created by: Silent-Mobius
#purpose: delete all partions on device
#date :19/10/2020
#
######################################

main(){
count=0
local disk_name="$1"
local disk_size=$(fdisk -l $disk_name|grep Disk|head -n 1|awk '{print $3}'|awk -F. '{print $1}')
local small_part=$(( disk_size/3 ))
local big_part=$(( disk_size/3*2 ))
local part_count=3

if [[ $# -lt  ]];then
    deco "Please provice disk name, disk size  --> EXACTLY in this order"
    exit 1
else

    parts=$(check_amount_of_parttions $disk_name)
    while [[ $parts -lt $count ]]
        do
            delete_part $disk_name
            let count++
        done

    deco "Done deleting partitions"
    count=0
    while [[ $part_count -gt $count ]] || 
        do
            if [[ $part_count -eq 2 ]];then 
                create_part $disk_name $big_part
                break
            else
                create_part $disk_name $small_part
                continue
            fi
            let count++
        done

    deco "Done creating the partions"
    count=0
    while [[ $part_count -gt $count ]]
        do
            mkfs_on_disk_mount $disk_name
            let count++
        done
    deco "Done"

  
fi
}

deco(){
    _time=2.5
    l="####################"
    clear
    printf "$l\n# %s\n$l" "$@"
    sleep $_time
    clear
}

help(){

if [[ -z $@ ]];then
   deco  "please provide disk name or disk size or both"
   exit 1
fi
}

delete_part(){
local disk_name="$@"
if [[ -z $disk_name ]];then
    help
    exit 1
else
fdisk $disk_name << EOL
d

w
EOL

fi
}


create_part(){
local disk_name="$1"
local disk_size="$2"

if [[ -z $disk_name ]] && [[ -z $disk_size ]];then
    help
    exit 1
else
fdisk $disk_name << EOL
n
p


+$disk_size
w
EOL

fi

}



check_amount_of_parttions(){
    local disk_name="$1"
    if [[ -z $disk_name ]];then
        help
        exit 1
    else
        amount_of_partitions=$(fdisk  -l $disk_name|grep  -A100 "Device"|wc -l)
        echo $amount_of_partitions
    fi
}

mkfs_on_disk_mount(){
    local disk_name="$@"
    _time=2.5
    if [[ -z $disk_name ]];then
        deco "unable to mount: no disk provided"
        exit 1
    else
        disk_part=$(fdisk -l $disk_name|awk '{print $1}'|grep '/dev')
        mkfs.ext4 $disk_part
        sleep $_time
        mkdir -p /mnt/$disk_part
        mount $disk_part /mnt$disk_part

    fi
}

###########!!!!!!!!DO NOT REMOVE !!!!!!!!#####################
main "$@"
