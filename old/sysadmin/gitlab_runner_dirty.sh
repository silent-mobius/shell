#!/usr/bin/env bash

if [[ $EUID -ne 0 ]];then
	echo not root
	exit
else

	useradd dep
	passwd dep << EOF
n1njaP$r4D!s3
EOF
	cd /home/dep
	ssh-keygen -t rsa -b 4096
	cd /home/dep/.ssh
	chown dep:dep -R id_rsa*
	
	yum install -y wget 
	rpm -ivh https://gitlab-runner-downloads.s3.amazonaws.com/latest/rpm/gitlab-runner_amd64.rpm
	if [[ $? -ne 0 ]];then
		echo somethig went wrong
		echo retrying to reinstall gitlab runner
		sleep 3
		wget https://gitlab-runner-downloads.s3.amazonaws.com/latest/rpm/gitlab-runner_amd64.rpm
		yum install -y ./gitlab-runner_amd64.rpm
	fi
	
	systenctl status gitlab-runner &> /dev/null
		if [[ $? -ne 0 ]];then
			echo something went wrong;
			echo check gitlab-runner service
			sleep 3
			exit
		fi

fi
