#!/usr/bin/env bash 
#set -x
#######################################
#created by: Silent-Mobius
#purpose: delete all partions on device
#date :19/10/2020
#
######################################

main(){
count=0
local disk_name="$@"
if [[ -z $disk_name ]];then
    help
    exit 1
else
parts=$(check_amount_of_parttions $disk_name)
while [[ $parts>$count ]]
    do
        delete_part $disk_name
        let count++
    done
deco "Done deleting partitions"

mkfs_on_disk_mount $disk_name

fi
}

deco(){
_time=2.5
l="####################"
clear
printf "$l\n# %s\n$l" "$@"
sleep $_time
clear
}

help(){
if [[ -z $@ ]];then
   deco  "please provide disk name"
   exit 1
fi
}

delete_part(){
local disk_name="$@"
if [[ -z $disk_name ]];then
    help
    exit 1
else
fdisk $disk_name << EOL
d

w
EOL

fi
}


check_amount_of_parttions(){
local disk_name="$1"
if [[ -z $disk_name ]];then
    deco "Unable to check mount please provide disk name"
    exit 1
else
    amount_of_partitions=$(fdisk  -l $disk_name|grep  -A100 "Device"|wc -l)
    echo $amount_of_partitions
fi
}


mkfs_on_disk_mount(){
local disk_name="$@"
if [[ -z $disk_name ]];then
    deco "Unable to mount: please provide disk name"
    exit 1
else
    mkfs.ext4 $disk_name
    sleep $_time
    mkdir -p /mnt/$disk_name
    mount $disk_name /mnt/$disk_name

fi
}
###########!!!!!!!!DO NOT REMOVE !!!!!!!!#####################
main "$@"
