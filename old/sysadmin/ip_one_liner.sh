#!/usr/bin/env bash
#
#################################################
#created by : Silent-Mobius
#purpose: one liners to print interface and ip address
#version: v1.0.0
#date:    19/12/2020
################################################

ip_addrs=($(ip a s |grep -vE "lo|::1|inet6"|grep UP|awk '{print $2}'))
inets=($(ip a s |grep -vE "127|::1|inet6"|grep -E "inet"|awk '{print $2}'))


i=0;
while [[ $i -le ${#d[@]} ]];
	do 
		printf "%s --> %s\n" ${ip_addrs[i]} ${inets[i]} 
		((i++))
	done
 
