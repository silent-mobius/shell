#!/usr/bin/env bash

sed -i 's/SELINUX=enforcing/SELINUX=permissive/' /etc/selinux/config

yum update -y

yum install -y epel-release

yum install -y git htop python3-pip nginx

pip3 install flask gunicorn

useradd flask -s /sbin/nologin

echo "[Unit]
Description=django daemon
After=network.target

[Service]
User=nginx
Group=nginx
WorkingDirectory=/home/django/slides
ExecStart=/usr/local/bin/gunicorn --workers 3 --bind unix:/srv/slides/slides.sock slides.wsgi:application

[Install]
WantedBy=multi-user.target" > /etc/systemd/system/flask.service


echo 'server {
    listen 80;
    server_name your-server-ip

    location = /favicon.ico { access_log off; log_not_found off; }
    location /static/ {
        root /srv/slides;
    }

    location / {
        proxy_set_header Host $http_host;
        proxy_set_header X-Real-IP $remote_addr;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header X-Forwarded-Proto $scheme;
        proxy_pass http://unix:/srv/slides/slides.sock;
    }
}' > /etc/nginx/conf.d/flask.conf 

chown nginx:nginx -R etc/nginx/conf.d/flask.conf 

cd /home/django

git clone https://gitlab.com/silent-mobius/slides5.git /srv/slides

chown nginx:nginx -R /srv/slides

systemctl enable --now flask.serivce
systemctl enable --now nginx.serivce
clear
reboot
